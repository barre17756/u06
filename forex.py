from forex_python.converter import CurrencyRates

c = CurrencyRates()

amount = 200

from_currency = 'USD'

to_currency = 'SEK'

convert_amount = c.convert(from_currency, to_currency, amount)

print ( f'{amount} {from_currency} is equal to {convert_amount} {to_currency}')