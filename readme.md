==================================================
Your Flask Application Name
==================================================

.. image:: logo.png
   :alt: Your Flask Application Logo
   :align: right

Introduction
------------

Your Flask Application Name is a web application that allows users to perform money transfers between senders and receivers. This documentation provides an overview of the application's features, installation instructions, and usage guidelines.

Features
--------

- User registration and authentication
- Dashboard for authenticated users to manage their account
- Money transfer functionality between different currencies

Installation
------------

1. Clone the repository:

   ::

       $ git clone https://github.com/barre17756/chasacademy

2. Navigate to the project directory:

   ::

       $ cd your-flask-app

3. Create a virtual environment:

   ::

       $ python -m venv venv

4. Activate the virtual environment:

   ::

       $ source venv/bin/activate

5. Install the required dependencies:

   ::

       $ pip install -r requirements.txt

Usage
-----

1. Start the Flask development server:

   ::

       $ python app.py

2. Access the application in your web browser at ``http://localhost:5000``.

3. Navigate to the different pages of the application:

   - Home Page: Displays a welcome message and basic information about the application.
   - Sign Up: Allows users to create a new account by providing their details.
   - Login: Enables users to authenticate themselves using their email and password.
   - Dashboard: Shows user-specific information and provides access to various account management features.
   - Send Money: Allows users to initiate money transfers between different currencies.



