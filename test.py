import pytest
from flask import session
from  app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_home_page(client):
    response = client.get('/')
    assert response.status_code == 200
    assert b"Welcome to the Home Page" in response.data

def test_signup(client):
    response = client.post('/signup', data={
        'first_name': 'John',
        'last_name': 'Doe',
        'email': 'john@example.com',
        'phone': '1234567890',
        'date_of_birth': '1990-01-01',
        'password': 'password',
        'repeat_password': 'password'
    })
    assert response.status_code == 200
    assert b"Sign up successful" in response.data

def test_login(client):
    response = client.post('/login', data={
        'email': 'john@example.com',
        'password': 'password'
    })
    assert response.status_code == 302  # Redirect to dashboard
    assert session['user_id'] is not None
    assert session['first_name'] == 'John'

def test_dashboard_authenticated(client):
    with client.session_transaction() as sess:
        sess['user_id'] = 1
        sess['first_name'] = 'John'

    response = client.get('/dashboard')
    assert response.status_code == 200
    assert b"Welcome, John! This is your dashboard." in response.data

def test_dashboard_unauthenticated(client):
    response = client.get('/dashboard')
    assert response.status_code == 302  # Redirect to login

def test_send_money_authenticated(client):
    with client.session_transaction() as sess:
        sess['user_id'] = 1

    response = client.post('/send-money', data={
        'from_country': 'USD',
        'to_country': 'EUR'
    })
    assert response.status_code == 200
    assert b"USD is equal to" in response.data

def test_send_money_unauthenticated(client):
    response = client.post('/send-money', data={
        'from_country': 'USD',
        'to_country': 'EUR'
    })
    assert response.status_code == 302  # Redirect to login
